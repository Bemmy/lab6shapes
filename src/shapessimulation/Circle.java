/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapessimulation;

public class Circle extends Shape{
        
    public Circle(double value){
        this.value = value;
    }
    
    public double getArea(double radius){
        
        return Math.PI * (Math.pow(radius, 2));
    }
    
    public double getPerimeter(double radius){
        return 2* Math.PI * radius;
    }
    
}
