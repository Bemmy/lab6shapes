/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapessimulation;

public abstract class Shape {
    
    protected double value;
    
    public Shape(){}
    
    public double getValue(){
        return value;
    }
    
    public abstract double getArea(double d);
    
    public abstract double getPerimeter(double d);
    
}
