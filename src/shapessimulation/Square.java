/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapessimulation;

public class Square extends Shape {
        
    public Square(double value){
        this.value = value;
    }
    
    public double getArea(double side){
        
        return side * side;
    }
    
    public double getPerimeter(double side){
        
        return 4 * side;
    }
    
}
