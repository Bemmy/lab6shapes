/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapessimulation;

import java.util.*;

public class ShapesSimulation {
    
    public static void main(String[] args) {
        
        Shape circle = new Circle(8);
        Shape square = new Square(6);
        
        List<Shape> shapes = new ArrayList<>();
        shapes.add(circle);
        shapes.add(square);
        
        for(Shape shp: shapes){
            
            System.out.printf("Area: %.2f Perimeter: %.2f\n",
                    shp.getArea(shp.getValue()), 
                    shp.getPerimeter(shp.getValue()));
        }
        
    }
    
}
